#!/bin/bash

if [ $EXISTS -eq 1 ]; then
    echo "Push de l'image docker hub sur la registry locale ($TAG)."
    docker tag $DOCKER_HUB_IMAGE $TAG
    docker push $TAG
fi


