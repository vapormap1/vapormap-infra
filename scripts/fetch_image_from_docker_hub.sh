#!/bin/bash

echo $EXISTS

if [ $EXISTS -eq 1 ]; then
    echo "L'image $DOCKER_HUB_IMAGE n'est pas présente dans la registry locale."
    echo "Lancement de la recuperation de l'image depuis Docker Hub."
    docker pull $DOCKER_HUB_IMAGE
    docker images
else
    echo "L'image est déjà dans le registry. Rien à faire."
fi
